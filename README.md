# How to not make a doom bot
# -or-
# How I Ended Up Starting to Reimplement DOOM in Python

So I installed [freedoom](https://freedoom.github.io/) and it wasn't long before I wanted some good ol' fashion deathmatch.

My internet is limit and the number of people still playing are limited.

So I looked for a bot... found only some bot embedded in [ZDoom](https://www.zdoom.org).

I thought well, it couldn't be that hard to write a program that sends packets to a server
to simulate a player...

If only I had done research ahead time and read this [doomwiki page](https://doomwiki.org/wiki/Doom_networking_component)

I picked Python because it's a scripting level, clean, high-ish level, stable, robust standard library and it follows the tradition paradigm of C/C++/Java
which would be useful when I'd be ripping snippets of C code and trying to reuse them.

Knowing what I do now, I would have opt'ed for have the C client or game engine make call outs to a scripting language a la
[guile](https://www.gnu.org/software/guile/) or [lua](https://www.lua.org/)
Or turning the client+engine+wad parts into a library and use Python's [ffi](https://docs.python.org/3/library/ctypes.html)
to hook itself into the guts of the C engine.

Well I didn't know so I dove in a kept started poking around `d_client.c` and `d_server.c`.

Getting Python to do the basic hand shake of joining, confirming and starting a game was relatively easy.

My biggest problem was that I had forgotten how to read C pointer (there are no concrete structs for the packets, just the header from `protocol.h`
and pointer logic in the client and server) and forgotten some of Python's syntax.

Cool, knocked off the rust and then I looked around for WAD library and found
the very nice [omgifol](https://github.com/devinacker/omgifol) written in Python.

Then the moment happened... oh crap that's how it works...

Uh... I looked around for DOOM in Python... I can't be the only crazy person.

I found [PyDoom](https://github.com/Pink-Silver/PyDoom) but it was barely started and was working from the GUI side inwards.

In despiration I looked around other non-C implementations and
found [mochadoom](https://github.com/AXDOOMER/mochadoom) a maybe-working Java port... but no networking.

And that's where I stop (for now).

What does this bot do...

Joins a game, loads the map, says 'lol' every 100 tics or so.
If the bot is hit, the [prboom+](http://prboom-plus.sourceforge.net) will halt, hitting some crazy cheat detection.

Download the repo and modify the paths, exe names and wads in `play.sh`.

Run it, goof around and watch the failure.

As for me, I'm not sure the next stop.

I looks like there is a need for a bot... I still want one.

* Do I keep with Python and help out PyDoom?
* Do I jump into Java land and bring my protocol / client parts into mocha?
* Do I keep with C and see which scripting language I can shove into it?

Time will tell...

bot-a-bing, bot-o-doom
