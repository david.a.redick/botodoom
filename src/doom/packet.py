# Copyright (C) 1999 by
# id Software, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman
# Copyright (C) 1999-2000 by
# Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze
# Copyright 2005, 2006 by
# Florian Schulze, Colin Phipps, Neil Stevens, Andrey Budko
# Copyright 2020 by
# David A. Redick - Translation into Python3.5
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.


# DOOM is little endian
import struct

from doom import ticcmd

# this is a pointer to the module object instance itself.
import sys
this = sys.modules[__name__]

# From protocol.h enum packet_type_e
# initial packet to server
# "player to server, i want to join"
# server will response with PKT_SETUP
# redick has seen that if a single player/client sends rapidly sends these from same address
# then server will think that its two people and will alloc two player infos for it
this.PKT_INIT=0

# game information packet
# from the server to the player as a response to PKT_INIT
# "ok you want to play, here's the wad and map etc and settings that we're playing"
# See d_server.c
# The server sends the packet...server sleeps...and server sends it again.
#
# see protocol.h setup_packet_s for layout
#
# All the while the players that already connected are spamming with PKT_GO
this.PKT_SETUP=1

# game has started
# like everyone including has to say go before things start.
# twice... one to say ready and then again to confirm
# then server responds with it's own PKT_GO and we're playing the game
this.PKT_GO=2

#  tics from a client/player to server
# see d_ticcmd.h for structure of the data (not including the packet header and count and player id)
# or in our case ticcmd.py
this.PKT_TICC=3

# tics from server
# see d_server.c
# after packet header
# some byte which redick doesn't understand.
# byte of number records
# -- the records --
# byte of player id
# the ticcmd of that player
this.PKT_TICS=4

# Request for retransmission
# when server sends this - the tic in the header is apparently the bad packet or ticcmd that server wants
#
# from d_client.c - NetUpdate
# when a player gets PKT_TICS
# and it see that there is a gap in the tics
# the player can ask server to resend.
# in this case there is an extra byte for the requesting
# player id
this.PKT_RETRANS=5

# Extra info packet
# can be from client/player to server
# - player picks a color
# - player wants to save a game
#
# and server to players
# the server does no real processing just broadcasts it to the other players
#
# from d_client.c
# after packet header
# 'type' - from netmisctype_t in d_net.h
# an enum of nm_plcolour = 3, nm_savegamename = 4
# has note about leaving space and low value being used setup packets
# LittleLong(consoleplayer);
# LittleLong(len);
# and bytes of data
#
# g_game.c actually calls the D_NetSendMisc
this.PKT_EXTRA=6

# Player quit game
# client to server
# and then server to clients
this.PKT_QUIT=7

# Server telling the players it was shut down
this.PKT_DOWN=8

# Wad file request
this.PKT_WAD=9

# Request for client back-off
# The server sends this when we have got ahead of the other clients. We should
# stall the input side on this client, to allow other clients to catch up.
this.PKT_BACKOFF=10

# From protocol.h packet_header_t
def make_header(packet_type, tic=0):
	packet = {}

	# byte Simple checksum of the entire packet
	packet['checksum'] = 0x00

	# byte Type of packet from the packet_type_e enum
	packet['type'] = packet_type

	# unsigned long Timestamp
	packet['tic'] = tic
	return packet

def make_init():
	return make_header(this.PKT_INIT)

def make_go_player(consoleplayer):
	p = make_header(this.PKT_GO)
	p['consoleplayer'] = consoleplayer
	return p

def make_player_quit(consoleplayer):
	p = make_header(this.PKT_QUIT)
	p['consoleplayer'] = consoleplayer
	return p

def make_from_client_tics(consoleplayer, cmd, tic=0):
	p = make_header(this.PKT_TICC, tic)
	p['sendtics'] = 1
	p['consoleplayer'] = consoleplayer
	p['localcmds'] = cmd
	return p

# a lot from d_server.c and d_client.c
def pack(p):
	data = pack_header(p)
	ptype = p['type'];
	if (this.PKT_INIT == ptype):
		# done
		return data
	elif (this.PKT_SETUP == ptype):
		# todo
		return data
	elif (this.PKT_GO == ptype):
		data.append(p['consoleplayer'])
		return data
	elif (this.PKT_TICC == ptype):
		data.append(p['sendtics']) # byte 'sendtics' the number of ticcmd_t obj being sent
		data.append(p['consoleplayer'])
		data = data + p['localcmds'].pack(); # and then a list of ticcmd_t objs from localcmds
		return data
	elif (this.PKT_TICS == ptype):
		# todo
		return data
	elif (this.PKT_RETRANS == ptype):
		# todo
		return data
	elif (this.PKT_EXTRA == ptype):
		# todo
		return data
	elif (this.PKT_QUIT == ptype):
		data.append(p['consoleplayer'])
		return data
	elif (this.PKT_DOWN == ptype):
		# todo
		return data
	elif (this.PKT_WAD == ptype):
		# todo
		return data
	elif (this.PKT_BACKOFF == ptype):
		# todo
		return data
	elif (this.PKT_INIT == ptype):
		# todo
		return data
	else:
		print('unknown packet type {}'.format(ptype))
	return data


def pack_header(packet):
	my_bytearray = bytearray()
	my_bytearray.append(packet['checksum'])
	my_bytearray.append(packet['type'])
	my_bytearray.append(0x00) # documented as reserved in the packet header
	my_bytearray.append(0x00) # documented as reserved in the packet header
	my_bytearray = my_bytearray + bytearray(struct.pack('<L', packet['tic']))
	return my_bytearray


# You should verify before unpacking.
# return chunk
# [0] is packet dict
# [1] is remaining bytes
# a lot from d_server.c and d_client.c
def unpack(my_bytearray):
	chunk = unpack_header(my_bytearray)
	ptype = chunk[0]['type'];
	if (this.PKT_INIT == ptype):
		# done
		return chunk
	elif (this.PKT_SETUP == ptype):
		return unpack_setup(chunk)
	elif (this.PKT_GO == ptype):
		# done
		return chunk
	elif (this.PKT_TICC == ptype):
		print('TODO PKT_TICC')
		return chunk
	elif (this.PKT_TICS == ptype):
		return unpack_server_tics(chunk)
	elif (this.PKT_RETRANS == ptype):
		# done
		return chunk
	elif (this.PKT_EXTRA == ptype):
		return unpack_extra(chunk)
	elif (this.PKT_QUIT == ptype):
		chunk[0]['consoleplayer'] = chunk[1][0]
		return (chunk[0], chunk[1][1:])
	elif (this.PKT_DOWN == ptype):
		# done
		return chunk
	elif (this.PKT_WAD == ptype):
		print('TODO PKT_WAD')
		return chunk
	elif (this.PKT_BACKOFF == ptype):
		print('TODO PKT_WAD')
		return chunk
	elif (this.PKT_INIT == ptype):
		print('TODO PKT_INIT')
		return chunk
	else:
		print('unknown packet type {}'.format(ptype))
	return chunk

def unpack_header(my_bytearray):
	packet = {}
	packet['checksum'] = my_bytearray[0]
	packet['type'] = my_bytearray[1]
	# reserved
	# reserved
	packet['tic'] = struct.unpack_from('<L', my_bytearray, 4)[0]

	# cddddddddr
	# list-tail bytes 8
	remaining = my_bytearray[8:]
	return (packet, remaining)

# chunk is tuple unpack_header
# see protocol.h setup_packet_s for layout
def unpack_setup(chunk):
	packet = chunk[0]
	my_bytearray = chunk[1]

	packet['players'] = my_bytearray[0]
	packet['yourplayer'] = my_bytearray[1]
	packet['skill'] = my_bytearray[2]
	packet['episode'] = my_bytearray[3]
	packet['level'] = my_bytearray[4]
	packet['deathmatch'] = my_bytearray[5]
	packet['complevel'] = my_bytearray[6]


	# https://doomwiki.org/wiki/Ticdup
	# CC BY-SA 4.0 International
	# Ticdup (tic duplication) is a feature of the Doom networking engine which allows the bandwidth used by the game to be reduced.
	#
	# In a normal Doom game, the game clock (see tic) runs at 35 frames per second. In a network game, this means that the players'
	# inputs are sampled 35 times per second. These are placed in structures (called ticcmds), and transmitted to the other computers
	# in the game.
	#
	# If the ticdup option is used, the sampling occurs at a reduced rate. For example, if a ticdup of 2 is used, the sample rate is
	# reduced to 17.5 times per second (half the rate). As a result, the amount of data transmitted per second is reduced by half.
	# The computers in the game then "double up" the received ticcmd structures: i.e. the same ticcmd is used for consecutive tics.
	#
	# The drawback to this system is the reduced movement accuracy. The reduced sampling rate means that the player has less precise
	# control over their movement. The graphical framerate is also reduced.
	packet['ticdup'] = my_bytearray[7]

	# from man prboom-plus-game-server(6)
	# This causes extra information to be sent with each network packet; this will help on networks with high packet loss,
	# but will use more bandwidth
	#
	# https://doomwiki.org/wiki/Extratic
	# CC BY-SA 4.0 International
	#
	# Extratic is an option in the Doom networking engine which helps reduce the effect of packet loss on the game.
	# In a normal Doom game, the players' input is sampled 35 times per second (see tic). These inputs are placed in structures
	# called ticcmds. The ticcmd structures are sent as packets to the other computers in the game.
	#
	# If a packet is lost, the receiving machine transmits a resend request to the sending machine. Under Doom, the game cannot
	# advance until all players have received the ticcmd for the next tic to be run. Packet loss therefore causes a short stall
	# in the game. This affects the perceived "smoothness" of the game.
	#
	# Over a large-scale network such as the Internet, packet loss is a common occurrence and therefore a serious issue to multiplayer
	# games. If the extratic option is enabled, the Doom engine includes the previously transmitted ticcmd when sending a new ticcmd.
	# The result is that if a single packet is lost (the most likely occurrence), it will not have such a large effect on the game, as
	# the next packet will contain the same ticcmd, removing the need to retransmit.
	#
	# The drawback to the extratic option is that each ticcmd structure is effectively transmitted twice, increasing the amount of
	# bandwidth needed. In the worst-case scenario, the bandwidth needed doubles (although this is often not the case as the header
	# data of the encapsulating protocol is not affected).
	packet['extratic'] = my_bytearray[8]

	my_bytearray = my_bytearray[9:]
	packet['game_options'] = my_bytearray[:64] # GAME_OPTIONS_SIZE == 64

	my_bytearray = my_bytearray[64:]
	packet['numwads'] = my_bytearray[0]
	packet['wadnames'] = my_bytearray[1:] # ugh... probably wrong
	return (packet, None)

# chunk is tuple unpack_header
# from d_server.c
def unpack_server_tics(chunk):
	packet = chunk[0]
	my_bytearray = chunk[1]

	# always seems to be 1
	#packet['some byte that redick doesnt understand'] = my_bytearray[0]

	num = packet['number_of_records'] = my_bytearray[1]
	my_bytearray = my_bytearray[2:] # cddr

	packet['records'] = []
	for i in range(num):
		record = {}

		record['player_id'] = my_bytearray[0]
		my_bytearray = my_bytearray[1:] # cdr

		record['ticcmd'] = ticcmd.unpack(my_bytearray)
		my_bytearray = my_bytearray[ticcmd.sizeof:]

		packet['records'].append(record)

	# should be empty bytearray
	if (len(my_bytearray) > 0):
		print('unpack_server_tics has left over data')

	return (packet, my_bytearray)

# chunk is tuple from unpack_header
def unpack_extra(chunk):
	packet = chunk[0]
	my_bytearray = chunk[1]

	packet['netmisctype_t'] = struct.unpack_from('<I', my_bytearray, 0)[0]
	# temp convert the enum value into a human friendly text
	if (3 == packet['netmisctype_t']):
		packet['netmisctype_t'] = 'nm_plcolour'
	elif (4 == packet['netmisctype_t']):
		packet['netmisctype_t'] = 'nm_savegamename'
	else:
		print('unsupported netmisctype_t enum value', packet['netmisctype_t'])

	# NOTE: in the code it's signed but the server only supports 4 players
	packet['consoleplayer'] = struct.unpack_from('<I', my_bytearray, 4)[0]

	packet['len_of_data'] = struct.unpack_from('<I', my_bytearray, 8)[0]

	# and bytes of data
	packet['data'] = my_bytearray[12:]

	# sanity check
	if (packet['len_of_data'] == len(packet['data'])):
		pass
	else:
		print('unpack_extra data len mismatch')

	return (packet, None)

# convert the given dict to some bytes that are ready to send over wire
def for_udp(packet):
	data = pack(packet)
	data[0] = make_checksum(data)
	return data

def verify(my_bytearray):
	check = make_checksum(my_bytearray)
	if (check == my_bytearray[0]):
		return True
	return False

# From i_network.c ChecksumPacket
def make_checksum(my_bytearray):
	size = len(my_bytearray)
	if (0 == size or 1 == size):
		return 0


	# Skip the first byte because we don't want to calculate the checksum in the checksum
	skip_first = True
	sum = 0
	for b in my_bytearray:
		if (skip_first):
			skip_first = False
		else:
			sum = sum + b
			# wrap the value to fit with the byte range limit
			# in C the unsign char type does this behind the scenes.
			if (sum >= 256):
				sum = sum - 256
	return sum
