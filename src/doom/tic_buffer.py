# Copyright 2020 by David A. Redick
# Released under the terms of the GNU AGPLv3
#
# In this was was implemented in a couple spots as a fixed length array
# with scattered mod logic so that it cycles around.
#
# See:
# d_client.c - ticcmd_t localcmds[BACKUPTICS];
# d_client.c - ticcmd_t netcmds[MAXPLAYERS][BACKUPTICS];
#
# redick thought this is where OOP is handy,
#
# note that this defaults to BACKUPTICS but can be any size.
#
# also note that the original code stored ticcmd
# this data structure can store anything but it does require:
# a tic and the data associated with it.

from collections import deque
from pprint import pformat

class tic_buffer:
	# doomstat.h - BACKUPTICS 12
	def __init__(self, my_maxlen=12):
		# the left (front) most are the oldest records
		# the right (rear) most are the newest records
		#
		# NOTE: tic are byte sized it is possible to have
		# have the counter inuitive case of records with
		# a tic pattern: [ 254, 255, 0, 1, 2 ... ]
		self.my_dequeue = deque(maxlen=my_maxlen)

	def __repr__(self):
		# code friendly
		# aka type this and you'll get same obj
		# well the dict print doesn't like that so whatever
		return pformat(vars(self), indent=4, width=1)

	def __str__(self):
		# human friendly string (dict style)
		return pformat(vars(self), indent=4, width=1)

	# data is typically a ticcmd but can be anything.
	def save(self, my_tic, my_data):
		self.my_dequeue.append({ 'tic': my_tic, 'data' : my_data })

	def get(self, my_tic):
		for record in self.my_dequeue:
			if (my_tic == record['tic']):
				return record['data']
		return None

