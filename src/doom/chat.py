# Copyright 2020 by David A. Redick
# Released under the terms of the GNU AGPLv3

from doom import ticcmd


class chat_handler:
	"""
	Saving up the chat characters is rather annoying to do in client.py
	So this keeps up with the chatchar from all players
	"""
	def __init__(self, max_players=4):
		self.players = []
		for i in range(max_players):
			self.players.append('')

	def get(self, from_player_id):
		return self.players[from_player_id]

	# Returns None when no message was complete.
	# Returns the string when message was complete
	def save(self, from_player_id, chatchar):
		# Looks like they all start with this.
		if (5 == chatchar):
			self.players[from_player_id] = ''
		elif (13 == chatchar):
			# \r
			return self.players[from_player_id]
		else:
			self.players[from_player_id] = self.players[from_player_id] + chr(chatchar)



def say_to_all(text):
	"""Generate all the ticcmds to talk as if hitting the 'T' key."""

	my_ticcmds = []

	cmd = ticcmd.ticcmd()
	cmd.chatchar = 5
	my_ticcmds.append(cmd)

	for letter in text:
		cmd = ticcmd.ticcmd()
		cmd.chatchar = ord(letter)
		my_ticcmds.append(cmd)

	cmd = ticcmd.ticcmd()
	cmd.chatchar = 13
	my_ticcmds.append(cmd)

	return my_ticcmds


