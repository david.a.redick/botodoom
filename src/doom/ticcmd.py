# Copyright (C) 1999 by
# id Software, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman
# Copyright (C) 1999-2000 by
# Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze
# Copyright 2005, 2006 by
# Florian Schulze, Colin Phipps, Neil Stevens, Andrey Budko
# Copyright 2020 by
# David A. Redick - Translation into Python3.5
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

from pprint import pformat
import struct

# Mostly from d_ticcmd.h
# struct ticcmd_t

# The data sampled per tick (single player)
# and transmitted to other peers (multiplayer).
# Mainly movements/button commands per game tick,
# plus a checksum for internal state consistency.
# CPhipps - explicitely signed the elements, since they have to be signed to work right
class ticcmd:
	def __init__(self, forwardmove=0, sidemove=0, angleturn=0, consistancy=0, chatchar=0x00, buttons=0x00):
		# https://doomwiki.org/wiki/Tic
		# CC BY-SA 4.0 International
		#
		# - forwardmove and sidemove record the signed net total of the various
		# forward/backward and right/left controls (each total limited to no more than 50 and no less than -50).
		#
		# - angleturn records the net total angle of turning motion controls.
		#
		# - consistancy (sic) is normally the player's x-position, and is used to confirm that network games are in sync.
		#
		# - chatchar can be a character to be added to the interplayer message area on the heads-up display.
		#
		# - buttons encodes the following actions: attack, use, change weapon (including new weapon number),
		# pause, and save game (including savegame slot number).

		# signed char
		# *2048 for move
		# If greater than TURBOTHRESHOLD=0x32 (50) then you'll be flagged by server as cheating (turbohack)
		self.forwardmove = forwardmove

		# signed char
		# *2048 for move
		self.sidemove = sidemove

		# signed short
		# <<16 for angle delta
		self.angleturn = angleturn

		# short
		# checks for net game
		# see g_game.c for the calculations
		self.consistancy = consistancy

		# unsigned char
		self.chatchar = chatchar

		# unsigned char
		self.buttons = buttons

	def __repr__(self):
		# code friendly
		# aka type this and you'll get same obj
		# well the dict print doesn't like that so whatever
		return pformat(vars(self), indent=4, width=1)

	def __str__(self):
		# human friendly string (dict style)
		return pformat(vars(self), indent=4, width=1)

	def pack(self):
		# DOOM is little endian
		my_bytearray = bytearray(struct.pack('<b', self.forwardmove))
		my_bytearray = my_bytearray + bytearray(struct.pack('<b', self.sidemove))
		my_bytearray = my_bytearray + bytearray(struct.pack('<h', self.angleturn))
		my_bytearray = my_bytearray + bytearray(struct.pack('<h', self.consistancy))

		if (str == type(self.chatchar)):
			my_bytearray.append(ord(self.chatchar))
		else:
			my_bytearray.append(self.chatchar)

		my_bytearray.append(self.buttons)
		return my_bytearray

	def has_chatchar(self):
		return 0x00 != self.chatchar


# a friendly helper
sizeof = 8

# an unpacking constructor-ish
# seemed friendlier for caller than method or actual constructor
# From d_server.c
def unpack(my_bytearray):
	cmd = ticcmd()
	cmd.forwardmove = struct.unpack_from('<b', my_bytearray, 0)[0]
	cmd.sidemove = struct.unpack_from('<b', my_bytearray, 1)[0]
	cmd.angleturn = struct.unpack_from('<h', my_bytearray, 2)[0]
	cmd.consistancy = struct.unpack_from('<h', my_bytearray, 4)[0]
	cmd.chatchar = my_bytearray[6]
	cmd.buttons = my_bytearray[7]
	return cmd

