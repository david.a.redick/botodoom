# Copyright 2020 by David A. Redick
# Released under the terms of the GNU AGPLv3
#
# This is going to get complex
# wrapper around the omgifol objects.
# that library is more map editor / level creation focused
# redick need more game focused

from pprint import pformat
from omg import WAD
from omg import MapEditor

class wad_wrapper:
	def __init__(self, path_to_wad):
		self.wad = WAD()
		print('loading WAD', path_to_wad)
		self.wad.from_file(path_to_wad)
		# raw lumps
		self.map = None
		# the parsed map data
		self.editor = None
		# redick - same C name
		self.deathmatchstarts = None

	# maplevel
	# some people call it a 'map'
	# other people call it a 'level'
	def load_map(self, episode, maplevel):
		map_id = 'E{}M{}'.format(episode, maplevel)
		print('loading map', map_id)
		self.map = self.wad.maps[map_id]
		print('parsing map')
		self.editor = MapEditor(self.map)

		#print('load map', map_id, self.map)
		# where are the starts??

		# raw data
		#print('THINGS', self.map['THINGS'])

		# that make_struct is funky and redick can't figure out how to do __str__ pretty printing
		#print('things', self.editor.things)
		# errors out.
		#print(pformat(vars(self.editor.things), indent=4, width=1))
		# generic class and address
		#print('things[0]', self.editor.things[0])
		# works
		#print(pformat(vars(self.editor.things[0]), indent=4, width=1))

		self.deathmatchstarts = []
		for thing in self.editor.things:
			if (11 == thing.type):
				#print(pformat(vars(thing), indent=4, width=1))
				self.deathmatchstarts.append(thing)

