# Copyright (C) 1999 by
# id Software, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman
# Copyright (C) 1999-2000 by
# Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze
# Copyright 2005, 2006 by
# Florian Schulze, Colin Phipps, Neil Stevens, Andrey Budko
# Copyright 2020 by
# David A. Redick - Translation into Python3.5
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

# Game client
# Mostly from d_client.c
# Some i_network.c
# Some from g_game.c
#
# prefix naming convention:
# doom_*  doom specific stuff
# my_*    redick's bot or debugging stuff
# all others necessary code and python specific stuff

import socket
import time
from doom import packet
from doom import tic_buffer
from doom import chat
from doom import game


class client:
	def __init__(self, bot=None, address='127.0.0.1', port=5030):
		# DOOM netgames talks over UDP
		self.sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
		# non-blocking receives
		self.sock.settimeout(0.0)
		# need to keep this to send data.
		self.binding = (address, port)

		# The last chunk received
		# [0] is the packet dict
		# [1] is the remaining unmapped raw data
		self.my_chunk = None

		self.my_last_ticcmd = None

		self.my_bot = bot

		# The way DOOM was design as a peer to peer checking
		# The network controls the flow of the game.
		self.doom_game = game.game()

		# From protocol.h - struct setup_packet_s
		self.doom_setup = None

		# When we get a PKT_TICS,
		# we save the received tic commands
		# in buffers, one for each player
		self.doom_netcmds = [
			tic_buffer.tic_buffer(),
			tic_buffer.tic_buffer(),
			tic_buffer.tic_buffer(),
			tic_buffer.tic_buffer()
		]

		# Locally generated tic commands.
		# Saved off incase the server wants us to resend one.
		self.doom_localcmds = tic_buffer.tic_buffer()

		self.my_chat_handler = chat.chat_handler()

	def doom_send_packet(self, p, check_save=True):
		"""basically analogous to i_network.c code"""

		#print('tic', p['tic'])

		#if (packet.PKT_TICC == p['type']):
			#if (p['localcmds'].has_chatchar()):
			#	print("I'm typing", p)
	

		# check if we need to save this off
		if (True == check_save and packet.PKT_TICC == p['type']):
			self.doom_localcmds.save(p['tic'], p)

		data = packet.for_udp(p)
		self.sock.sendto(data, self.binding)

	# basically analogous to i_network.c code
	def doom_receive_packet(self, fixed_size):
		try:
			# pair tuple of (bytes, address)
			my_pair = self.sock.recvfrom(fixed_size)
			data = my_pair[0]
			v = packet.verify(data)
			if not v:
				print('packet checksum failed to verify')
				self.my_chunk = (None, data)
				return None
			else:
				self.my_chunk = packet.unpack(data)
				remaining_data = self.my_chunk[1]
				if (remaining_data is not None and len(remaining_data) > 0):
					print('packet', self.my_chunk[0])
					print('remaining_data', remaining_data)
				return self.my_chunk[0]
		except BlockingIOError:
			# no data is fine.
			return None

	def doom_send_init(self):
		print('doom_send_init')
		p = packet.make_init()
		self.doom_send_packet(p)

	def doom_send_player_go(self):
		print('doom_send_player_go')
		p = packet.make_go_player(self.doom_setup['yourplayer'])
		self.doom_send_packet(p)

	def doom_send_player_quit(self):
		#print('doom_send_player_quit')
		p = packet.make_player_quit(self.doom_setup['yourplayer'])
		self.doom_send_packet(p)

	# handy for testing without a server
	def set_dummy_doom_setup(self):
		self.doom_setup = {
			'tic': 0,
			'game_options': b'\x01\x01\x00\x01\x00\x01\x00\x00\x00\x01^\xef\x9fs\x01\x00\x00\x00\x00\x80\x00\x01\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00',
			'checksum': 3,
			'ticdup': 1,
			'complevel': 17,
			'wadnames': b'\x00',
			'episode': 1,
			'skill': 1,
			'yourplayer': 1,
			'deathmatch': 1,
			'numwads': 0,
			'extratic': 0,
			'type': 1,
			'level': 1,
			'players': 2
		}

	def run(self):
		current_milli_time = lambda: int(round(time.time() * 1000))

		#last = now = current_milli_time()

		state = 'doom_send_init'
		#state = 'play'
		#self.set_dummy_doom_setup()
		while (True):
			state = self._do_sending_state(state, self.doom_game.gametic)

			p = self.doom_receive_packet(1024)
			if (None == p):
				#print('no packet read')
				pass
			elif (packet.PKT_GO == p['type']):
				# NOTE: prbroom+ and other ports sends the go twice
				# from d_server.c and d_client.c - it looks like the intent was to be sure it was lost per UDP
				if (self.doom_game.playing):
					# already got it.
					pass
				else:
					state = 'play'
					self.doom_game.playing = True
					print('play time!')
					print('TODO loads of bot code')

					# todo player's color
					# g_game.c G_Ticker

					# spawn all the players
					for i in range(self.doom_setup['players']):
						self.doom_game.death_match_spawn_player(i)
			elif (packet.PKT_SETUP == p['type']):
				# NOTE: prbroom+ and other ports sends the setup twice
				# apparently intended for two step ack
				if (None == self.doom_setup):
					#print('got setup', p)
					self.doom_setup = p;
					print('I am player {}'.format(self.doom_setup['yourplayer']))
					self.doom_game.wad.load_map(self.doom_setup['episode'], self.doom_setup['level'])
					self.doom_send_player_go()
					state = 'doom_send_player_go'
				else:
					#print('got second setup should match first', p)
					pass
			elif (packet.PKT_DOWN == p['type']):
				print('server was shut down')
				break;
			elif (packet.PKT_QUIT == p['type']):
				print('Player {} left the game'.format(p['consoleplayer']));
				print('I will quit too!')
				self.doom_send_player_quit()
				break;
			elif (packet.PKT_EXTRA == p['type']):
				print('exra', p);
			elif (packet.PKT_TICS == p['type']):
				self.save_ticcmds_from_server(p)
				state='play'
			elif (packet.PKT_RETRANS == p['type']):
				old_tic = p['tic']
				print('server wants retrans of', old_tic)
				old = self.doom_localcmds.get(old_tic)
				print('what we stored', old)
				if (old is None):
					print('SOL server')
				else:
					self.doom_send_packet(old, check_save=False)
			else:
				print('huh?', p)

		self.sock.close
		self.sock = None

	# the doom protocol is designed where we spam the server till we get a response
	# given that it's UDP it's understandable
	# for sanity make state == the name of the method to call
	def _do_sending_state(self, state, tick):
		if ('doom_send_init' == state):
			# tell the server we want to play
			# we have to keep screaming because the client may have been started before the server
			self.doom_send_init();
			# unfortunately since bots are typically on the same ip as the server
			# we have to do it slowly because the server might think we're two different players
			time.sleep(5)
		elif ('doom_send_player_go' == state):
			# spam the server till everyone is ready.
			time.sleep(1)
			self.doom_send_player_go()
		elif ('play' == state):
			if (None == self.my_bot):
				pass
			else:
				cmd = self.my_bot.update(tick)
				p = packet.make_from_client_tics(self.doom_setup['yourplayer'], cmd, tick)
				self.doom_send_packet(p)
			state = 'wait'
		else:
			# if we're doing nothing... then give cpu a rest
			# magic is from some doom wiki or something.
			time.sleep(0.02)
		return state

	# p is a PKT_TICS structure
	def save_ticcmds_from_server(self, p):
		#print('TICS', p)
		from_tic = p['tic']
		for record in p['records']:
			from_player = record['player_id']
			self.doom_netcmds[from_player].save(from_tic, record['ticcmd'])

			if (record['ticcmd'].has_chatchar()):
				message = self.my_chat_handler.save(from_player, record['ticcmd'].chatchar)
				if (message is None):
					pass
				else:
					# Someone said something...
					print('Player {} says {}'.format(from_player, message))

		self.doom_game.gametic = from_tic + 1

