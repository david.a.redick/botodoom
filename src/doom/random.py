# Copyright (C) 1999 by
# id Software, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman
# Copyright (C) 1999-2000 by
# Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze
# Copyright 2005, 2006 by
# Florian Schulze, Colin Phipps, Neil Stevens, Andrey Budko
# Copyright 2020 by
# David A. Redick - Translation into Python3.5
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.


# PRNG
# From m_random.h and m_random.c
#
# Due to the importance of this working 100% like the C code
# redick is keeping all the old school C names so that cross reference
# will be as straight forward as possible.
#
# Redick also kept all the comments.


# killough 1/19/98: rewritten to use to use a better random number generator
# in the new engine, although the old one is available for compatibility.
#
# killough 2/16/98:
#
# Make every random number generator local to each control-equivalent block.
# Critical for demo sync. Changing the order of this list breaks all previous
# versions' demos. The random number generators are made local to reduce the
# chances of sync problems. In Doom, if a single random number generator call
# was off, it would mess up all random number generators. This reduces the
# chances of it happening by making each RNG local to a control flow block.
#
# Notes to developers: if you want to reduce your demo sync hassles, follow
# this rule: for each call to P_Random you add, add a new class to the enum
# type below for each block of code which calls P_Random. If two calls to
# P_Random are not in "control-equivalent blocks", i.e. there are any cases
# where one is executed, and the other is not, put them in separate classes.

# killough 2/16/98:  We always update both sets of random number
# generators, to ensure repeatability if the demo_compatibility
# flag is changed while the program is running. Changing the
# demo_compatibility flag does not change the sequences generated,
# only which one is selected from.
#
# All of this RNG stuff is tricky as far as demo sync goes --
# it's like playing with explosives :) Lee



# Keep all current entries in this list the same, and in the order
# indicated by the #'s, because they're critical for preserving demo
# sync. Do not remove entries [reorder] simply because they become unused later.
# These are the enum pr_class_t
pr_skullfly=0
pr_damage=1
pr_crush=2
pr_genlift=3
pr_killtics=4
pr_damagemobj=5
pr_painchance=6
pr_lights=7
pr_explode=8
pr_respawn=9
pr_lastlook=10
pr_spawnthing=11
pr_spawnpuff=12
pr_spawnblood=13
pr_missile=14
pr_shadow=15
pr_plats=16
pr_punch=17
pr_punchangle=18
pr_saw=19
pr_plasma=20
pr_gunshot=21
pr_misfire=22
pr_shotgun=23
pr_bfg=24
pr_slimehurt=25
pr_dmspawn=26
pr_missrange=27
pr_trywalk=28
pr_newchase=29
pr_newchasedir=30
pr_see=31
pr_facetarget=32
pr_posattack=33
pr_sposattack=34
pr_cposattack=35
pr_spidrefire=36
pr_troopattack=37
pr_sargattack=38
pr_headattack=39
pr_bruisattack=40
pr_tracer=41
pr_skelfist=42
pr_scream=43
pr_brainscream=44
pr_cposrefire=45
pr_brainexp=46
pr_spawnfly=47
pr_misc=48
pr_all_in_one=49
pr_opendoor=50
pr_targetsearch=51
pr_friends=52
pr_threshold=53
pr_skiptarget=54
pr_enemystrafe=55
pr_avoidcrush=56
pr_stayonlift=57
pr_helpfriend=58
pr_dropoff=59
pr_randomjump=60
pr_defect=61
NUMPRCLASS=62


# each element is unsigned char
rndtable = (
    0,   8, 109, 220, 222, 241, 149, 107,  75, 248, 254, 140,  16,  66 ,
    74,  21, 211,  47,  80, 242, 154,  27, 205, 128, 161,  89,  77,  36 ,
    95, 110,  85,  48, 212, 140, 211, 249,  22,  79, 200,  50,  28, 188 ,
    52, 140, 202, 120,  68, 145,  62,  70, 184, 190,  91, 197, 152, 224 ,
    149, 104,  25, 178, 252, 182, 202, 182, 141, 197,   4,  81, 181, 242 ,
    145,  42,  39, 227, 156, 198, 225, 193, 219,  93, 122, 175, 249,   0 ,
    175, 143,  70, 239,  46, 246, 163,  53, 163, 109, 168, 135,   2, 235 ,
    25,  92,  20, 145, 138,  77,  69, 166,  78, 176, 173, 212, 166, 113 ,
    94, 161,  41,  50, 239,  49, 111, 164,  70,  60,   2,  37, 171,  75 ,
    136, 156,  11,  56,  42, 146, 138, 229,  73, 146,  77,  61,  98, 196 ,
    135, 106,  63, 197, 195,  86,  96, 203, 113, 101, 170, 247, 181, 113 ,
    80, 250, 108,   7, 255, 237, 129, 226,  79, 107, 112, 166, 103, 241 ,
    24, 223, 239, 120, 198,  58,  60,  82, 128,   3, 184,  66, 143, 224 ,
    145, 224,  81, 206, 163,  45,  63,  90, 168, 114,  59,  33, 159,  95 ,
    28, 139, 123,  98, 125, 196,  15,  70, 194, 253,  54,  14, 109, 226 ,
    71,  17, 161,  93, 186,  87, 244, 138,  20,  52, 123, 251,  26,  36 ,
    17,  46,  52, 231, 232,  76,  31, 221,  84,  37, 216, 165, 212, 106 ,
    197, 242,  98,  43,  39, 175, 254, 145, 190,  84, 118, 222, 187, 136 ,
    120, 163, 236, 249
)

rngseed = 1993

# The rng's state
# redick - Due to name clash, change name was struct rng_t
class doom_rng:
	def __init__(self):
		# unsigned int
		# Each block's random seed
		self.seed = []
		for i in range(NUMPRCLASS):
			self.seed.append(0)

		# signed int
		self.rndindex = 0

		# signed int
		self.prndindex = 0

	# As M_Random, but used by the play simulation.
	# Returns a number from 0 to 255

	# redick - Due to the globals vs parameters and defaults I removed M_Random just pass in pr_class=pr_misc
	# redick - In C there was an extra macro to support call flow tracing via the where_from parameters
	# redick - !!! In C gametic, basetic are global counters but now pass them in
	# redick - !!! In C demo_insurance is global flag defaults to 1/True in m_misc.h/.c and g_game.c for the bot we don't care about it.
	# redick - !!! In C demo_insurance is global flag and fuck if I could fine it.  For the bot I doubt it matters
	def P_Random(self, pr_class, gametic, basetic, demo_compatibility=True, demo_insurance=True, where_from=''):
		# killough 2/16/98:  We always update both sets of random number
		# generators, to ensure repeatability if the demo_compatibility
		# flag is changed while the program is running. Changing the
		# demo_compatibility flag does not change the sequences generated,
		# only which one is selected from.
		#
		# All of this RNG stuff is tricky as far as demo sync goes --
		# it's like playing with explosives :) Lee

		print('{} >>> pr_class={}, gametic={}, basetic={}\n'.format(where_from, pr_class+1, gametic, basetic))

		# signed int
		# redick - this bit and'ing was in the C code.
		self.rndindex = (self.rndindex + 1) & 255
		compat = self.rndindex
		if (pr_class == pr_misc):
			# redick - this bit and'ing was in the C code.
			self.prndindex = (self.prndindex + 1) & 255
			compat = self.prndindex

		# unsigned long
		boom = 0

		# killough 3/31/98:
		# If demo sync insurance is not requested, use
		# much more unstable method by putting everything
		# except pr_misc into pr_all_in_one

		# killough 3/31/98
		if (pr_class != pr_misc and not demo_insurance):
			pr_class = pr_all_in_one

		boom = self.seed[pr_class]

		# killough 3/26/98: add pr_class*2 to addend
		self.seed[pr_class] = boom * 1664525 + 221297 + pr_class*2
		# redick - doing the and'ing to pack into C storage class
		self.seed[pr_class] = self.seed[pr_class] & 0xffffffff

		if (demo_compatibility):
			return rndtable[compat]

		boom = boom >> 20

		# killough 3/30/98: use gametic-levelstarttic to shuffle RNG
		# killough 3/31/98: but only if demo insurance requested,
		# since it's unnecessary for random shuffling otherwise
		# killough 9/29/98: but use basetic now instead of levelstarttic
		# cph - DEMOSYNC - this change makes MBF demos work,
		# but does it break Boom ones?
		if (demo_insurance):
			boom = boom + ((gametic - basetic) * 7);

		# redick - this bit and'ing was in the C code.
		return boom & 255;


	# Fix randoms for demos.
	# Initialize all the seeds
	#
	# This initialization method is critical to maintaining demo sync.
	# Each seed is initialized according to its class, so if new classes
	# are added they must be added to end of pr_class_t list. killough
	def M_ClearRandom(self):
		# add 3/26/98: add rngseed
		# unsigned int
		# redick - doing the and'ing to pack into C storage class
		seed = (rngseed*2+1) & 0xffffffff

		# go through each pr_class and set
		# each starting seed differently
		for i in range(NUMPRCLASS):
			# redick - doing the and'ing to pack into C storage class
			seed = (seed * 69069) & 0xffffffff
			self.seed[i] = seed

		# clear two compatibility indices
		self.prndindex = 0
		self.rndindex = 0

