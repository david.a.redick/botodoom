# Copyright (C) 1999 by
# id Software, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman
# Copyright (C) 1999-2000 by
# Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze
# Copyright 2005, 2006 by
# Florian Schulze, Colin Phipps, Neil Stevens, Andrey Budko
# Copyright 2020 by
# David A. Redick - Translation into Python3.5
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#
# DESCRIPTION: Main game control interface.
# g_game.h and g_game.c

from doom import random
from doom import wad_wrapper

class game():
	def __init__(self):
		self.rng = random.doom_rng()
		self.wad = wad_wrapper.wad_wrapper('/usr/share/games/doom/freedoom1.wad')

		# The next game tic to be run.
		# see:
		# d_client.c
		# d_main.c
		# d_game.c
		self.gametic = 0

		self.playing = False

	def death_match_spawn_player(self, playernum):
		"""
		G_DeathMatchSpawnPlayer
		Spawns a player at one of the random death match spots
		called at level load and each death

		redick in C this a void function, in Python I'm returning
		the index of the starting location and the map thing
		"""

		# redick in the original there was check for retard WADs.
		# redick going to make the assumption that human player can figure that out.

		num_starts = len(self.wad.deathmatchstarts)

		# TODO what is the basetic
		start_index = self.rng.P_Random(random.pr_dmspawn, self.gametic, 0) % num_starts

		# the C code does that call to G_CheckSpot
		# which is necessary far too frequent so big TODO there.
		start = self.wad.deathmatchstarts[start_index];

		# redick - I guess mark it as owned.
		# Odd that +1 all the other player code is 0 based.
		start.type = playernum+1;

		print('Player {} spawned at x={}, y={}'.format(playernum, start.x, start.y))

		return (start_index, start)

