# Copyright 2020 by David A. Redick
# Released under the terms of the GNU AGPLv3

from collections import deque
from pprint import pformat
from doom import ticcmd
from doom import chat

# while some bleed over into the doom client
# is unavoidable, the bot should only talk to the client
# in the form of ticcmds to (inject/process) and send to the server
#
# having the game engine talk to the bot will be more tricky
# the client will have to register the bot as some sort of listener/handler
class bot:
	def __init__(self):
		self.my_ticcmds = deque()
		self.next_talk_tick = 100;

	# main loop call
	# returns a list of ticcmds
	def update(self, tick):
		if (tick >= self.next_talk_tick):
			self.say_to_all('lol')
			self.next_talk_tick = tick + 1000

		if (len(self.my_ticcmds) > 0):
			return self.my_ticcmds.popleft();


		# Nothing queued up so tell the server the bot is doing nothing.
		return ticcmd.ticcmd()

	def say_to_all(self, text):
		"""Talk to everyone like the 'T' command in the game.  Note this implementation, the bot can't talk and play at the same time."""
		cmds = chat.say_to_all(text)
		for r in cmds:
			self.my_ticcmds.append(r)



