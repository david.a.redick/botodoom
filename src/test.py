#!/usr/bin/python3

# Copyright 2020 by David A. Redick
# Released under the terms of the GNU AGPLv3

import bot
from doom import client


my_client = client.client(bot.bot())
my_client.run()
