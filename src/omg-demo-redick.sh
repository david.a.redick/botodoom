#!/bin/sh

# The original PIL is dead.
# The fork with same module names is:
# pip install Pillow
# python3 -m pip install Pillow


# output file:
# E1M1_map.png
# python3 omg-demo-drawmaps.py /usr/share/games/doom/freedoom1.wad E1M1 1024 PNG


# fpv's version draws deathmatch starting locations, always png
# and is more verbose in logging
# output file:
# E1M1.png
python omg-demo-drawmaps.fpv.py -v -d /usr/share/games/doom/freedoom1.wad E1M1 2048 4.0


