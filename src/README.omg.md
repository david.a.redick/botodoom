omg - the module name for the omgifol project

Main Project Page:
https://github.com/devinacker/omgifol

Copying:
MIT License

Release Snap Shot:
https://github.com/devinacker/omgifol/releases/tag/0.4.0

See also:
https://pypi.org/project/omgifol/0.4.0/

While you can install the package and use it,
due to alpha nature of the bot code (and omifol) redick copied it en masse
for easy reference.

`pip install omgifol`

Or if you have both 2 and 3 installed.

`python3 -m pip install Pillow`

The files called omg-demo-*.py are from the omgifol demo/*.py script directory.
