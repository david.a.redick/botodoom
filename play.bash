#!/bin/bash

# Copyright 2020 by David A. Redick
# Released under the terms of the GNU AGPLv3

# redick tried sh
# but apparently the redirect syntax is different.
# yay defacto standard and the paper standard

# as redick was reverse engineering everything
# he had to add a bunch of custom fprintf's to
# prboom+ in addition to having an official
# install on the system hence the extra configuration

# Where ever you put the git repo
BOT_DIR=/home/redick/botodoom

# Debian
# BIN_DIR=/usr/games
BIN_DIR=/home/redick/prboom-installed/games

# Debian
#WAD_DIR=/usr/share/games/doom/
WAD_DIR=/home/redick/prboom-installed

${BIN_DIR}/prboom-plus-game-server -N 2 -d -v -v -n &> ${BOT_DIR}/server.log &

# At the moment the bot client is too agressive when joining
# so it tricks the service into registering the same client twice.
# this is expected since this is running both the bot client and real player client
# from the same IP
# So... start the real player client first as it's better

${BIN_DIR}/prboom-plus -iwad ${WAD_DIR}/freedoom1.wad -net localhost:5030 &> ${BOT_DIR}/game.log &


${BOT_DIR}/src/test.py &> ${BOT_DIR}/bot.log
